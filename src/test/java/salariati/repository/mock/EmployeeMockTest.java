package salariati.repository.mock;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

import static org.junit.Assert.*;

public class EmployeeMockTest {
    private EmployeeMock repo;

    @Before
    public void setUp() throws Exception {
        repo = new EmployeeMock();
    }

    @Test
    public void T02_WBT_Valid(){
        Employee oldEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.LECTURER, 200);
        repo.addEmployee(oldEmployee);
        int index = repo.getEmployeeList().indexOf(oldEmployee);
        Employee newEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.TEACHER, 350);
        assertFalse(repo.getEmployeeList().contains(newEmployee));
        repo.modifyEmployee(oldEmployee, newEmployee);
        assertTrue(repo.getEmployeeList().get(index).getFunction() == newEmployee.getFunction());
    }

    @Test
    public void T02_WBT_NonValid1(){
        Employee oldEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.LECTURER, 200);
        repo.addEmployee(oldEmployee);
        Employee newEmployee = null;
        repo.modifyEmployee(oldEmployee, newEmployee);
        assertFalse(repo.getEmployeeList().contains(newEmployee));
    }

    @Test
    public void T02_WBT_NonValid2(){
        Employee oldEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.LECTURER, 200);
        repo.addEmployee(oldEmployee);
        Employee newEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.LECTURER, 200);
        repo.modifyEmployee(oldEmployee, newEmployee);
        assertFalse(repo.getEmployeeList().contains(newEmployee));
    }

    @Test
    public void T02_WBT_NonValid3(){
        Employee oldEmployee = new Employee("Miriam", "1938886460041", DidacticFunction.LECTURER, 200);
        repo.addEmployee(oldEmployee);
        Employee newEmployee = new Employee("Miriam", "1938886460042", DidacticFunction.LECTURER, 200);
        repo.modifyEmployee(oldEmployee, newEmployee);
        assertFalse(repo.getEmployeeList().contains(newEmployee));
    }
}