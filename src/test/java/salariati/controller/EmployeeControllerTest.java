package salariati.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.mock.EmployeeMock;

import static org.junit.Assert.*;

public class EmployeeControllerTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private Employee employee1;
    private Employee employee2;
    private EmployeeController service;
    private EmployeeMock repo;
    private String lastName1;
    private String lastName2;

    @Before
    public void setUp() throws Exception {
        employee1 = new Employee("Moldovan", "1234567891234", DidacticFunction.TEACHER, 123);
        employee2 = new Employee("Petrean", "1234567891234", DidacticFunction.LECTURER, 103);
        repo = new EmployeeMock();
        service = new EmployeeController(repo);
        lastName1 = "Moldovan";
        lastName2 = "Petrean";
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void add1() throws EmployeeException {
        employee1.setSalary(1);
        assertEquals(service.addEmployee(employee1), false);
    }

    @Test
    public void add2() throws EmployeeException {
        assertEquals(service.addEmployee(employee2), true);
    }

    @Test
    public void add3() throws EmployeeException {
        employee1.setCnp("@123456789123");
        assertEquals(service.addEmployee(employee1), false);
    }

    @Test//(expected=EmployeeException.class)
    public void add4() throws EmployeeException {
        employee1.setSalary(0);
        assertEquals(service.addEmployee(employee1), false);
    }

    @Test//(expected=EmployeeException.class)
    public void add5() throws EmployeeException {
        employee2.setFunction(DidacticFunction.LECTURER);
        assertEquals(service.addEmployee(employee2), true);
    }

    @Test
    public void add6() throws EmployeeException {
        employee1.setLastName("Pinguin");
        assertEquals(service.addEmployee(employee1), true);
    }

    @Test
    public void add7() throws EmployeeException {
        employee2.setSalary(10232);
        assertEquals(service.addEmployee(employee2), true);
    }
}


